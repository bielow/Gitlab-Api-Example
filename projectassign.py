import requests
import json
import manageprojects

class member:
    gitlab_group = ""
    zedat_name = ""


    
def readStudentList(file_students):
    list = open(file_students)
    lines = list.read().splitlines()
    students = []
    for l in lines:
        itm = l.split('\t')
        if len(itm)!=2:
            raise Exception("Invalid student: '" + l + "'. Must have two items separated by tab!")
        s = member()
        s.zedat_name = itm[0]
        s.gitlab_group = itm[1]
        students.append(s)
    return students

def getStudentID(apilink, token, zedat_name):
    rawdata = requests.get(apilink + "/users?private_token=" + token + "&username=" + zedat_name, verify = True)
    data = json.loads(rawdata.content)
    s_id = None
    for d in data:
        if ("id" in d):
            s_id = d["id"]
            break
    if (s_id is None):
        print("Could not find student '" + zedat_name + "'.") # Response was: ", data)
    else:
        print("Found student id: ", s_id ,"\n")
        
    return s_id

def assign(apilink, token, group_name, file_students, access_level):
    students = readStudentList(file_students)
    for s in students:
        print("\n-- Student '" + s.zedat_name + "' in '" + s.gitlab_group + "'")
        if (s.gitlab_group == ''):
          print("\n-- Error: student '" + s.zedat_name + "' has no lab in KVV. Skipping.")
          continue
        proj_id = manageprojects.getprojectid(apilink, token, group_name, s.gitlab_group)
        if (proj_id is None):
            exit("Group is unknown. Please create it or correct its name")      # this is a hard error -- groups are not created correctly
        student_id = getStudentID(apilink, token, s.zedat_name)
        if (student_id is None):
            print("\n-- Error: student '" + s.zedat_name + "' is not known to Gitlab and was thus not assigned to '" + s.gitlab_group + "'")
            continue
        # creates member (does nothing if already exists)
        requests.post(apilink + "/projects/" + str(proj_id) + "/members?private_token=" + token + "&user_id=" + str(student_id) + "&access_level=" + str(access_level), verify = True)
        # does an update (e.g. access rights, in case the student already exists)
        link = apilink + "/projects/" + str(proj_id) + "/members/" + str(student_id) + "?private_token=" + token + "&access_level=" + str(access_level);
        requests.put(link, verify = True)
        print("   --> added")
