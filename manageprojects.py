import requests
import json

class project:
    id = 0
    name = ""

def create(apilink, token, groupid, fullname):
    print ("create project: " + fullname + "\n")
    requests.post(apilink + "/projects?private_token=" + token 
                                   + "&name=" + fullname
                                   + "&namespace_id=" + str(groupid),
                  verify = True)

                  
def remove(apilink, token, groupid, projid, name):
    requests.delete(apilink+"/projects/"+str(projid)+"?private_token="+token+"&namespace_id="+str(groupid), verify = True)
    print("Project " + name + " removed\n")




def getprojects(apilink, token, groupid, proj_name_prefix):
    print("warning: listing projects by Searching for a name prefix is broken in Gitlab, since the response will be incomplete for large answers. You might need to iterate (for deleting) or find a more specific prefix (which finds less hits)")
    rawdata = requests.get(apilink+"/groups/"+str(groupid)+"/projects?private_token="+token, verify = True)
    data = json.loads(rawdata.content)
    projects = []
    for i in range(0,len(data)):
        if (data[i]["name"].startswith(proj_name_prefix)):
            #print("got project: " + str(data[i]))
            prodata = project()
            prodata.id = data[i]["id"]
            prodata.name = data[i]["name"]
            projects.append(prodata)

    print("Found projects: " + ", ".join([p.name for p in projects]))
            
    return projects


def getprojectid(apilink, token, group_id, proj_name):
    # do NOT use the general query below, because it will prematurely stop after some records (some limitation/bug in the API)
    # this is problematic if you have a dozen or so groups
    #rawdata = requests.get(apilink+"/groups/" + str(groupid) + "/projects?private_token=" + token, verify = True)

    # query project directly using groupname/project (the / must be encoded as %2F)
    rawdata = requests.get(apilink + "/projects/" + str(group_id) + "%2F" + proj_name + "?private_token=" + token, verify = True)
    data = json.loads(rawdata.content)

    proj_id = None
    if ("name" in data and data["name"] == proj_name):
        proj_id = data["id"]
        print("Found project id: ", proj_id)
    else:
        print("Could not find project '" + proj_name + "'.")
        print("Response was: ", data)
        
    return proj_id
