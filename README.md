## Gitlab API example

Allows to
 - create projects within a group
 - delete projects within a group
 - batch-assign students to projects (via .tsv file)
  
 
Requirements
  - Python(3)
  - a manually created group in Gitlab
  - a corresponding Gitlab access token (available via environment variable $GITLABTOKEN) when running `main.py`

Get started
  - run `main.py`