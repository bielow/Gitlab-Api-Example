import requests
import json
import manageprojects
import projectassign
import os

token = os.getenv('GITLABTOKEN', None)
apilink = "https://git.imp.fu-berlin.de/api/v4/"
group_name = None

def getgroupid(group_name):

    rawdata = requests.get(apilink + "/groups?private_token=" + token, verify = True)
    data = json.loads(rawdata.content)

    group_id = None
    #print(data);
    #print (type(data), "\n")
    
    for d in data:
        if ("name" in d and d["name"] == group_name):
            group_id = d["id"] # int
            break
    if (group_id is None):
        print("Could not find group. Response was: ", data)
    else:
        print("Found group id: '", group_id ,"'\n")
    return group_id

def main():

    print("Welcome to your GitLab project setup\n")

    if (not token):
        raise Exception("Invalid authentication token: Set $GITLABTOKEN in your environment!")
    group_id = None
    group_name_default = "adp2023";                      
    while (group_id is None):
        group_name = input("Welche Gruppe soll bearbeitet werden (Enter = " + group_name_default + ")? ")
        if (not group_name):
            group_name = group_name_default
        group_id = getgroupid(group_name)

    while(True):

        print("\n")
        print("ls - list projects")
        print("cr - create projects")
        print("rm - remove projects")
        print("as - assign students to existing projects\n")

        
        eingabe = input("choice: ")
        
        if (eingabe == "ls"):
            proj_name_prefix = input("What is the project prefix? (Enter = all projects) ")
            projects = manageprojects.getprojects(apilink, token, group_id, proj_name_prefix)
    
        
        if(eingabe == "cr"):
            count = input("\nHow many projects do you need?: ")
            name = input("What is the project prefix (enter='group')?: ")
            if (not name):
                name = "group";
            sf = input("\nDo you really want 1.." + count + " projects with prefix '" + name + "'(if the projects already exist, nothing will happen)? (y/n): ")

            if(sf == "y" or sf == "yes"):
                # always pad to two digits (e.g. 01,02,...,10,)
                # If you have more projects, change the width accordingly
                # We do not compute it using log10, since adding more project later on could lead to Lab1 and Lab01, depending on count)
                width = 2
                for c in range(0, int(count)):
                    fullname = name + str(c+1).zfill(width)
                    manageprojects.create(apilink, token, group_id, fullname)
                print(str(count) + " projects created")

        if(eingabe == "rm"):
            proj_name_prefix = input("What is the project prefix (e.g. 'group')? (Enter = all projects) ")
            projects = manageprojects.getprojects(apilink, token, group_id, proj_name_prefix)
            
            sf = input("Do you really want to delete those? (y/n): ")
            if(sf == "y" or sf == "yes"):
                for p in projects:
                    #proj_id = manageprojects.getprojectid(apilink, token, group_id, p.name)
                    manageprojects.remove(apilink, token, group_id, p.id, p.name)

        if(eingabe == "as"):
            student_list = input("Aus welcher Liste sollen Nutzer hinzugefügt werden? (Enter = students.list)")
            if (not student_list):
                student_list = "students.list"
            access_level = input("Access-Level?\n10 => Guest access\n20 => Reporter access\n30 => Developer access (no push to master)\n40 => Maintainer access (Enter = Maintainer)")
            if (not access_level):
                access_level = 40;
            projectassign.assign(apilink, token, group_name, student_list, access_level)


main()
